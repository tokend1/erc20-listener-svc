FROM golang:1.20-alpine as buildbase

RUN apk add git build-base

WORKDIR /go/src/gitlab.com/tokend1/erc20-listener-svc

COPY vendor .
COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build  -o /usr/local/bin/erc20-listener-svc /go/src/gitlab.com/tokend1/erc20-listener-svc


FROM alpine:3.9

COPY --from=buildbase /usr/local/bin/erc20-listener-svc /usr/local/bin/erc20-listener-svc

RUN apk add --no-cache ca-certificates


