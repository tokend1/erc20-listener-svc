package main

import (
	"os"

	"gitlab.com/tokend1/erc20-listener-svc/internal/cli"
)

func main() {
	if !cli.Run(os.Args) {
		os.Exit(1)
	}
}
