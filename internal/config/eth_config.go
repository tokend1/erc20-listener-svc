package config

import (
	"github.com/pkg/errors"
	"gitlab.com/distributed_lab/figure"
	"gitlab.com/distributed_lab/kit/comfig"
	"gitlab.com/distributed_lab/kit/kv"
)

type TokenData struct {
	Endpoint     string
	ContractAddr string
	TokenDec     string
}

func NewToken(getter kv.Getter) Tokener {
	return &tokener{
		getter: getter,
	}
}

type Tokener interface {
	Token() TokenData
}

func NewTokener(getter kv.Getter) Tokener {
	return &tokener{
		getter: getter,
	}
}

type tokener struct {
	once   comfig.Once
	getter kv.Getter
}

func (c *tokener) Token() TokenData {
	return c.once.Do(func() interface{} {
		config := struct {
			Endpoint     string `fig:"endpoint,required"`
			ContractAddr string `fig:"contract_addr,required"`
			TokenDec     string `fig:"token_dec,required"`
		}{}

		err := figure.
			Out(&config).
			With(figure.BaseHooks).
			From(kv.MustGetStringMap(c.getter, "eth")).
			Please()
		if err != nil {
			panic(errors.Wrap(err, "failed to figure out eth"))
		}

		return TokenData{config.Endpoint, config.ContractAddr, config.TokenDec}
	}).(TokenData)
}
