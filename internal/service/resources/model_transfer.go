package resources

import (
	"time"
)

type Transfer struct {
	Hash     string
	Sender   string
	Receiver string
	Amount   string
	Time     time.Time
}
