package service

import (
	"context"
	"gitlab.com/tokend1/erc20-listener-svc/internal/data"
	"gitlab.com/tokend1/erc20-listener-svc/internal/data/pg"
	"log"
	"math/big"
	"strings"
	"sync"
	"time"

	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/ethclient"
	"gitlab.com/tokend1/erc20-listener-svc/internal/config"
	"gitlab.com/tokend1/erc20-listener-svc/internal/erc20"
)

type TransferEvent struct {
	Value *big.Int
}

func RunIngester(config config.Config) error {
	ethConfig := config.Token()

	client, err := ethclient.Dial(ethConfig.Endpoint)
	if err != nil {
		return err
	}

	contractAddress := common.HexToAddress(ethConfig.ContractAddr)
	usdtAbi, err := erc20.Erc20MetaData.GetAbi()
	if err != nil {
		return err
	}
	if err != nil {
		return err
	}

	query := ethereum.FilterQuery{
		Addresses: []common.Address{contractAddress},
		Topics:    [][]common.Hash{{usdtAbi.Events["Transfer"].ID}},
	}

	logs := make(chan types.Log)
	sub, err := client.SubscribeFilterLogs(context.Background(), query, logs)
	if err != nil {
		return err
	}

	db := pg.NewMasterQ(config.DB())

	var wg sync.WaitGroup
	wg.Add(1)

	go func() {
		defer wg.Done()
		for {
			select {
			case err := <-sub.Err():
				log.Fatal(err)
			case vLog := <-logs:
				event := new(TransferEvent)
				err := usdtAbi.UnpackIntoInterface(event, "Transfer", vLog.Data)
				if err != nil {
					log.Println("Error unpacking log data:", err)
					continue
				}

				hash := vLog.TxHash
				sender := strings.ToLower(common.HexToAddress(vLog.Topics[1].Hex()).String())
				receiver := strings.ToLower(common.HexToAddress(vLog.Topics[2].Hex()).String())
				tokens := event.Value

				transfer := data.Transfer{
					Hash:     hash.String(),
					Sender:   sender,
					Receiver: receiver,
					Amount:   tokens.String(),
					Time:     time.Now(),
				}

				_, err = db.Transfer().Insert(transfer)
				if err != nil {
					log.Println("Failed to insert:", err)
					continue
				}
			}
		}
	}()

	wg.Wait()

	return nil
}
