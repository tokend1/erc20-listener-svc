package service

import (
	"github.com/go-chi/chi"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/tokend1/erc20-listener-svc/internal/config"
	"gitlab.com/tokend1/erc20-listener-svc/internal/data/pg"
	"gitlab.com/tokend1/erc20-listener-svc/internal/service/handlers"
)

func (s *service) router(cfg config.Config) chi.Router {
	r := chi.NewRouter()

	r.Use(
		ape.RecoverMiddleware(s.log),
		ape.LoganMiddleware(s.log),
		ape.CtxMiddleware(
			handlers.CtxLog(s.log),
			handlers.CtxDB(pg.NewMasterQ(cfg.DB())),
		),
	)
	r.Route("/integrations/erc20-listener-svc", func(r chi.Router) {
		r.Get("/transfers", handlers.GetTransfers)
	})

	return r
}
