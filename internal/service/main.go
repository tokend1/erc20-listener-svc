package service

import (
	"net"
	"net/http"
	"sync"

	"gitlab.com/distributed_lab/kit/copus/types"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend1/erc20-listener-svc/internal/config"
)

type service struct {
	log      *logan.Entry
	copus    types.Copus
	listener net.Listener
	token    config.TokenData
	db       *pgdb.DB
}

func newService(cfg config.Config) *service {
	return &service{
		log:      cfg.Log(),
		copus:    cfg.Copus(),
		listener: cfg.Listener(),
		token:    cfg.Token(),
		db:       cfg.DB(),
	}
}

func (s *service) runApi(cfg config.Config, wg *sync.WaitGroup) error {
	defer wg.Done()
	r := s.router(cfg)

	if err := s.copus.RegisterChi(r); err != nil {
		return errors.Wrap(err, "cop failed")
	}
	s.log.Info("Api started")

	return http.Serve(s.listener, r)
}

func (s *service) runIngester(cfg config.Config, wg *sync.WaitGroup) error {
	defer wg.Done()
	err := RunIngester(cfg)
	if err != nil {
		return errors.Wrap(err, "Ingester failed")
	}
	s.log.Info("Ingester started")

	return err
}

func Run(cfg config.Config) {
	wg := &sync.WaitGroup{}

	svc := newService(cfg)

	wg.Add(2)
	go func() {
		err := svc.runApi(cfg, wg)
		if err != nil {
			panic(err)
		}
	}()
	go func() {
		err := svc.runIngester(cfg, wg)
		if err != nil {
			panic(err)
		}
	}()

	wg.Wait()
}
