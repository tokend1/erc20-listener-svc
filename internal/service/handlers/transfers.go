package handlers

import (
	"encoding/json"
	"gitlab.com/tokend1/erc20-listener-svc/internal/data"
	"gitlab.com/tokend1/erc20-listener-svc/internal/service/resources"
	"log"
	"net/http"
	"strings"
)

func TransfersToJson(transfers []data.Transfer) ([]byte, error) {
	var transfersJson []resources.Transfer
	for _, tr := range transfers {
		trJson := resources.Transfer{
			Hash:     tr.Hash,
			Sender:   tr.Sender,
			Receiver: tr.Receiver,
			Amount:   tr.Amount,
			Time:     tr.Time,
		}
		transfersJson = append(transfersJson, trJson)
	}
	return json.Marshal(transfersJson)
}

func GetTransfers(w http.ResponseWriter, r *http.Request) {
	db := DB(r)
	transferQ := db.Transfer()
	if r.URL.Query().Get("sender") != "" {
		transferQ = transferQ.FilterBySender(strings.ToLower(r.URL.Query().Get("sender")))
	}
	if r.URL.Query().Get("receiver") != "" {
		transferQ = transferQ.FilterByReceiver(strings.ToLower(r.URL.Query().Get("receiver")))
	}
	if r.URL.Query().Get("counterparty") != "" {
		transferQ = transferQ.FilterByCounterparty(strings.ToLower(r.URL.Query().Get("counterparty")))
	}
	transfers, err := transferQ.Select()
	if err != nil {
		log.Println("Failed to find:", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	log.Printf("Found %d transfers\n", len(transfers))
	out, err := TransfersToJson(transfers)
	if err != nil {
		log.Println("Failed to marshal:", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	_, err = w.Write(out)
	if err != nil {
		log.Println("Failed to write response:", err)
	}
}
