package handlers

import (
	"context"
	"gitlab.com/tokend1/erc20-listener-svc/internal/data"
	"net/http"

	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/tokend1/erc20-listener-svc/internal/config"
)

type ctxKey int

const (
	logCtxKey   ctxKey = iota
	dbCtxKey    ctxKey = 1
	tokenCtxKey ctxKey = 2
)

func CtxLog(entry *logan.Entry) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, logCtxKey, entry)
	}
}

func Log(r *http.Request) *logan.Entry {
	return r.Context().Value(logCtxKey).(*logan.Entry)
}

func CtxDB(entry data.MasterQ) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, dbCtxKey, entry)
	}
}

func DB(r *http.Request) data.MasterQ {
	return r.Context().Value(dbCtxKey).(data.MasterQ).New()
}

func CtxToken(entry *config.TokenData) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, tokenCtxKey, entry)
	}
}

func Token(r *http.Request) *config.TokenData {
	return r.Context().Value(tokenCtxKey).(*config.TokenData)
}
