-- +migrate Up

create table transfers
(
    id              bigserial primary key   not null,
    hash            char(67)                not null,
    sender          char(42)                not null,
    receiver        char(42)                not null,
    amount          numeric                 not null,
    time            timestamp without time zone
);

create index transfers_sender_index on transfers (sender);
create index transfers_receiver_index on transfers (receiver);
create index transfers_time_index on transfers (time);

-- +migrate Down
drop index transfers_sender_index;
drop index transfers_receiver_index;
drop index transfers_time_index;
drop table transfers;