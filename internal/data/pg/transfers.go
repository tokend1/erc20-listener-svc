package pg

import (
	"database/sql"
	sq "github.com/Masterminds/squirrel"
	"github.com/fatih/structs"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend1/erc20-listener-svc/internal/data"
)

const transfersTableName = "transfers"

func newTransferQ(db *pgdb.DB) data.TransferQ {
	return &transferQ{
		db:  db,
		sql: sq.StatementBuilder,
	}
}

type transferQ struct {
	db  *pgdb.DB
	sql sq.StatementBuilderType
}

func (q *transferQ) Get() (*data.Transfer, error) {
	var result data.Transfer
	err := q.db.Get(&result, q.sql.Select("*").From(transfersTableName))
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, errors.Wrap(err, "failed to get transfer from db")
	}
	return &result, nil
}

func (q *transferQ) Select() ([]data.Transfer, error) {
	var result []data.Transfer
	err := q.db.Select(&result, q.sql.Select("*").From(transfersTableName))
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, errors.Wrap(err, "failed to select transfers from db")
	}
	return result, nil
}

func (q *transferQ) Insert(value data.Transfer) (*data.Transfer, error) {
	clauses := structs.Map(value)

	var result data.Transfer
	stmt := sq.Insert(transfersTableName).SetMap(clauses).Suffix("returning *")
	err := q.db.Get(&result, stmt)
	if err != nil {
		return nil, errors.Wrap(err, "failed to insert transfer to db")
	}
	return &result, nil
}

func (q *transferQ) FilterByReceiver(address ...string) data.TransferQ {
	pred := sq.Eq{"receiver": address}
	q.sql = q.sql.Where(pred)
	return q
}
func (q *transferQ) FilterBySender(address ...string) data.TransferQ {
	pred := sq.Eq{"sender": address}
	q.sql = q.sql.Where(pred)
	return q
}

func (q *transferQ) FilterByCounterparty(address ...string) data.TransferQ {
	pred := sq.Or{
		sq.Eq{"receiver": address},
		sq.Eq{"sender": address}}
	q.sql = q.sql.Where(pred)
	return q
}
