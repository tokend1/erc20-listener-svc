package data

type MasterQ interface {
	New() MasterQ

	Transfer() TransferQ

	Transaction(fn func(db MasterQ) error) error
}
