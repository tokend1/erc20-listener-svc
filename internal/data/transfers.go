package data

import (
	"time"
)

type TransferQ interface {
	Get() (*Transfer, error)
	Select() ([]Transfer, error)
	Insert(value Transfer) (*Transfer, error)
	FilterByReceiver(address ...string) TransferQ
	FilterBySender(address ...string) TransferQ
	FilterByCounterparty(address ...string) TransferQ
}

type Transfer struct {
	ID       int64     `db:"id" structs:"-"`
	Hash     string    `db:"hash" structs:"hash"`
	Sender   string    `db:"sender" structs:"sender"`
	Receiver string    `db:"receiver" structs:"receiver"`
	Amount   string    `db:"amount" structs:"amount"`
	Time     time.Time `db:"time" structs:"time"`
}
